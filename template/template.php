<html>
<head>
  <link rel="stylesheet" href="css/style.css">
  <title>
    Data Mahasiswa
  </title>
</head>
<body>
  <div class="div-header">
    Data Mahasiswa
  </div>
  <div class="div-table">
    <div class="div-table-row">
      <div class="div-table-col"><a href="template.php?nav=home">Home</a></div>
      <div class="div-table-col"><a href="template.php?nav=info">Info</a></div>
    </div>
  </div>
  <?php
  if(isset($_GET['nav'])){
    if($_GET['nav'] == "info"){
      include 'info.php';
    }else{
      include 'home.php';
    }
  }else{
    include 'home.php';
  }
   ?>
  <div class="div-footer">
    Made with &#9829 in Ciwaruga by Ali Qornan Jaisyurrahman
  </div>
</body>
</html>
