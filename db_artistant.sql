-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2017 at 12:00 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_artistant`
--

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_title` text NOT NULL,
  `photo_shortdesc` text NOT NULL,
  `photo_fulldesc` text NOT NULL,
  `photo_image` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `photo_addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`photo_id`, `photo_title`, `photo_shortdesc`, `photo_fulldesc`, `photo_image`, `user_id`, `photo_addtime`) VALUES
(1, 'MonaLisa', 'The Mona Lisa is a half-length portrait of Lisa Gherardini by the Italian Renaissance artist Leonardo da Vinci, which has been described as "the best known, the most visited, the most written about.', 'The Mona Lisa (/?mo?n? ?li?s?/; Italian: Monna Lisa [?m?nna ?li?za] or La Gioconda [la d?o?konda], French: La Joconde [la ??k??d]) is a half-length portrait of Lisa Gherardini by the Italian Renaissance artist Leonardo da Vinci, which has been described as "the best known, the most visited, the most written about, the most sung about, the most parodied work of art in the world".\r\n\r\nThe painting is a portrait of Lisa Gherardini, the wife of Francesco del Giocondo, and is in oil on a white Lombardy poplar panel, and is believed to have been painted between 1503 and 1506. Leonardo may have continued working on it as late as 1517. It was acquired by King Francis I of France and is now the property of the French Republic, on permanent display at the Louvre Museum in Paris since 1797.\r\n\r\nThe subject''s expression, which is frequently described as enigmatic,[3] the monumentality of the composition, the subtle modelling of forms, and the atmospheric illusionism were novel qualities that have contributed to the continuing fascination and study of the work.', 'http://totallyhistory.com/wp-content/uploads/2011/06/Mona_Lisa1.jpg', 1, '2017-05-12 09:13:48'),
(2, 'The Starry Night', 'The Starry Night is an oil on canvas by the Dutch post-impressionist painter Vincent van Gogh. Painted in June 1889, it depicts the view from the east-facing window of his asylum room at Saint-Rémy-de-Provence', 'The Starry Night is an oil on canvas by the Dutch post-impressionist painter Vincent van Gogh. Painted in June 1889, it depicts the view from the east-facing window of his asylum room at Saint-Rémy-de-Provence, just before sunrise, with the addition of an idealized village. It has been in the permanent collection of the Museum of Modern Art in New York City since 1941, acquired through the Lillie P. Bliss Bequest. It is regarded as among Van Gogh''s finest works, and is one of the most recognized paintings in the history of Western culture.', 'http://totallyhistory.com/wp-content/uploads/2011/11/Van_Gogh_-_Starry_Night.jpg', 2, '2017-05-12 09:22:14'),
(3, 'The Last Supper', 'The Last Supper is a late 15th-century mural painting by Leonardo da Vinci in the refectory of the Convent of Santa Maria delle Grazie, Milan. It is one of the world''s most famous paintings', 'The Last Supper (Italian: Il Cenacolo [il t?e?na?kolo] or L''Ultima Cena [?lultima ?t?e?na]) is a late 15th-century mural painting by Leonardo da Vinci in the refectory of the Convent of Santa Maria delle Grazie, Milan. It is one of the world''s most famous paintings.\r\n\r\nThe work is presumed to have been commenced around 1495–1496 and was commissioned as part of a plan of renovations to the church and its convent buildings by Leonardo''s patron Ludovico Sforza, Duke of Milan. The painting represents the scene of The Last Supper of Jesus with his disciples, as it is told in the Gospel of John, 13:21. Leonardo has depicted the consternation that occurred among the Twelve Disciples when Jesus announced that one of them would betray him.\r\n\r\nDue to the methods used, and a variety of environmental factors, as well as intentional damage, very little of the original painting remains today, despite numerous restoration attempts, the last being completed in 1999.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/%C3%9Altima_Cena_-_Da_Vinci_5.jpg/1024px-%C3%9Altima_Cena_-_Da_Vinci_5.jpg', 1, '2017-05-12 09:26:56'),
(4, 'The Scream', 'The Scream is the popular name given to each of four versions of a composition, created as both paintings and pastels, by Norwegian Expressionist artist Edvard Munch between 1893 and 1910.', 'The Scream (Norwegian: Skrik) is the popular name given to each of four versions of a composition, created as both paintings and pastels, by Norwegian Expressionist artist Edvard Munch between 1893 and 1910. The German title Munch gave these works is Der Schrei der Natur (The Scream of Nature). The works show a figure with an agonized expression against a landscape with a tumultuous orange sky. Arthur Lubow has described The Scream as "an icon of modern art, a Mona Lisa for our time."\r\n\r\nEdvard Munch created the four versions in various media. The National Gallery in Oslo, Norway, holds one of two painted versions (1893, shown here). The Munch Museum holds the other painted version (1910, see gallery, below) and a pastel version from 1893. These three versions have not traveled for years.\r\n\r\nThe fourth version (pastel, 1895) was sold for $119,922,600 at Sotheby''s Impressionist and Modern Art auction on 2 May 2012 to financier Leon Black, the fourth highest nominal price paid for a painting at auction. The painting was on display in the Museum of Modern Art in New York from October 2012 to April 2013.\r\n\r\nAlso in 1895, Munch created a lithograph stone of the image. Of the lithograph prints produced by Munch, several examples survive. Only approximately four dozen prints were made before the original stone was resurfaced by the printer in Munch''s absence.\r\n\r\nThe Scream has been the target of several high-profile art thefts. In 1994, the version in the National Gallery was stolen. It was recovered several months later. In 2004, both The Scream and Madonna were stolen from the Munch Museum, and both were recovered two years later.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/The_Scream.jpg/800px-The_Scream.jpg', 3, '2017-05-12 09:29:14'),
(5, 'Tampan dan Berani', '', '', 'http://1.bp.blogspot.com/-Amkwc6OfYvs/Ud9-mMSYbrI/AAAAAAAABso/UgC7VmpBzN0/s1600/boldandbrash.jpg', 4, '2017-05-12 09:30:38'),
(6, 'Napoleon Crossing the Alps', 'Napoleon Crossing the Alps is the title given to the five versions of an oil on canvas equestrian portrait of Napoleon Bonaparte painted by the French artist Jacques-Louis David between 1801 and 1805.', 'Napoleon Crossing the Alps (also known as Napoleon at the Saint-Bernard Pass or Bonaparte Crossing the Alps) is the title given to the five versions of an oil on canvas equestrian portrait of Napoleon Bonaparte painted by the French artist Jacques-Louis David between 1801 and 1805. Initially commissioned by the King of Spain, the composition shows a strongly idealized view of the real crossing that Napoleon and his army made across the Alps through the Great St. Bernard Pass in May 1800.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/David_-_Napoleon_crossing_the_Alps_-_Malmaison2.jpg/300px-David_-_Napoleon_crossing_the_Alps_-_Malmaison2.jpg', 5, '2017-05-12 09:32:23'),
(7, 'The Storm on the Sea of Galilee', 'The Storm on the Sea of Galilee is a painting from 1633 by the Dutch Golden Age painter Rembrandt van Rijn that was in the Isabella Stewart Gardner Museum of Boston, Massachusetts, United States, prior to being stolen in 1990.', 'The Storm on the Sea of Galilee is a painting from 1633 by the Dutch Golden Age painter Rembrandt van Rijn that was in the Isabella Stewart Gardner Museum of Boston, Massachusetts, United States, prior to being stolen in 1990. The painting depicts the miracle of Jesus calming the storm on the Sea of Galilee, as depicted in the fourth chapter of the Gospel of Mark in the New Testament of the Christian Bible. It is Rembrandt''s only seascape.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Rembrandt_Christ_in_the_Storm_on_the_Lake_of_Galilee.jpg/800px-Rembrandt_Christ_in_the_Storm_on_the_Lake_of_Galilee.jpg', 6, '2017-05-12 09:33:23'),
(8, 'Girl with a Pearl Earring', 'Girl with a Pearl Earring is an oil painting by 17th-century Dutch painter Johannes Vermeer. It is a tronie of a girl with a headscarf and a pearl earring. The painting has been in the collection of the Mauritshuis in The Hague since 1902.', 'Girl with a Pearl Earring (Dutch: Meisje met de parel) is an oil painting by 17th-century Dutch painter Johannes Vermeer. It is a tronie of a girl with a headscarf and a pearl earring. The painting has been in the collection of the Mauritshuis in The Hague since 1902.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Meisje_met_de_parel.jpg/800px-Meisje_met_de_parel.jpg', 7, '2017-05-12 09:34:20'),
(9, 'The Persistence of Memory', 'The Persistence of Memory is a 1931 painting by artist Salvador Dalí, and is one of his most recognizable works.', 'The Persistence of Memory (Catalan: La persistència de la memòria) is a 1931 painting by artist Salvador Dalí, and is one of his most recognizable works. First shown at the Julien Levy Gallery in 1932, since 1934 the painting has been in the collection of the Museum of Modern Art (MoMA) in New York City, which received it from an anonymous donor. It is widely recognized and frequently referenced in popular culture, and sometimes referred to by more descriptive (though incorrect) titles, such as ''The Soft Watches'' or ''The Melting Watches''.', 'https://upload.wikimedia.org/wikipedia/en/d/dd/The_Persistence_of_Memory.jpg', 8, '2017-05-12 09:36:36'),
(10, 'Whistler''s Mother', 'Arrangement in Grey and Black No.1, best known under its colloquial name Whistler''s Mother, is a painting in oils on canvas created by the American-born painter James McNeill Whistler in 1871.', 'Arrangement in Grey and Black No.1, best known under its colloquial name Whistler''s Mother, is a painting in oils on canvas created by the American-born painter James McNeill Whistler in 1871. The subject of the painting is Whistler''s mother, Anna McNeill Whistler. The painting is 56.81 by 63.94 inches (144.3 cm × 162.4 cm), displayed in a frame of Whistler''s own design. It is exhibited in and held by the Musée d''Orsay in Paris, having been bought by the French state in 1891. It is one of the most famous works by an American artist outside the United States. It has been variously described as an American icon and a Victorian Mona Lisa. Whistler''s Mother is currently on display at the Art Institute of Chicago through May 21, 2017.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Whistlers_Mother_high_res.jpg/300px-Whistlers_Mother_high_res.jpg', 10, '2017-05-12 11:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_accountname` varchar(15) NOT NULL,
  `user_fullname` text NOT NULL,
  `user_password` text NOT NULL,
  `user_jointime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_accountname` (`user_accountname`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_accountname`, `user_fullname`, `user_password`, `user_jointime`) VALUES
(1, 'davinci', 'Leonardo da Vinci', 'aea862aa22a0f5b829d5795882889c08', '2017-05-12 11:11:51'),
(2, 'vangogh', 'Vincent van Gogh', 'f50aad4392af4912e7fc95161cb36df3', '2017-05-12 11:12:25'),
(3, 'edvard', 'Edvard Munch', '46e6f90a359cdf6256efa1366ea27f5e', '2017-05-12 11:13:04'),
(4, 'squidward', 'Squidward Tentacles', '8200fdece927b56ffcfeae7518d5df61', '2017-05-12 11:13:25'),
(5, 'louis', 'Jacques-Louis David', '777cadc280bb23ebea268ded98338c39', '2017-05-12 11:13:51'),
(6, 'rembrandt', 'Rembrandt', '3fcbec0a16da5dd4e59aba38fbaab1a0', '2017-05-12 11:14:20'),
(7, 'john', 'Johannes Vermeer', '527bd5b5d689e2c32ae974c6229ff785', '2017-05-12 11:14:43'),
(8, 'salvador', 'Salvador Dali', '5a3b26725e9bcc107fd0120638944823', '2017-05-12 11:15:09'),
(9, 'rene', 'René Magritte', '93de1a7f9a00f8823ac377738b66236b', '2017-05-12 11:20:25'),
(10, 'james', 'James Abbott McNeill Whistler', 'b4cc344d25a2efe540adbf2678e2304c', '2017-05-12 11:48:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
