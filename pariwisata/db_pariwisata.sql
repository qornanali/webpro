-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 05, 2017 at 12:03 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pariwisata`
--

-- --------------------------------------------------------

--
-- Table structure for table `gambar_obyek`
--

CREATE TABLE IF NOT EXISTS `gambar_obyek` (
  `gambar_obyek_kd` int(11) NOT NULL AUTO_INCREMENT,
  `obyek_kd` int(11) NOT NULL,
  `gambar_obyek_link` text NOT NULL,
  PRIMARY KEY (`gambar_obyek_kd`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `gambar_obyek`
--

INSERT INTO `gambar_obyek` (`gambar_obyek_kd`, `obyek_kd`, `gambar_obyek_link`) VALUES
(1, 1, 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Patenggang_Lake_panorama%2C_Bandung_Regency%2C_2014-08-21.jpg/500px-Patenggang_Lake_panorama%2C_Bandung_Regency%2C_2014-08-21.jpg'),
(2, 2, 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Gedung-Sate-Trees.jpg/1280px-Gedung-Sate-Trees.jpg'),
(3, 2, 'https://media-cdn.tripadvisor.com/media/photo-s/0b/5a/a0/86/gedung-sate.jpg'),
(4, 3, 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Kawah_Putih_Lake_from_the_viewing_platform%2C_Bandung_Regency%2C_2014-08-21.jpg/800px-Kawah_Putih_Lake_from_the_viewing_platform%2C_Bandung_Regency%2C_2014-08-21.jpg'),
(5, 3, 'https://www.initempatwisata.com/mediafiles/2015/03/Tangga-jalan-Kawah-Putih-Ciwidey.jpg'),
(6, 4, 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Tangkuban_Parahu.jpg/800px-Tangkuban_Parahu.jpg'),
(7, 4, 'https://upload.wikimedia.org/wikipedia/commons/a/a9/TangkubanPerahu.jpg'),
(8, 5, 'http://www.petaasia.com/wp-content/uploads/2013/06/Bandung-Zoo-Indonesia-April-19-2013-34.jpg'),
(9, 5, 'https://media-cdn.tripadvisor.com/media/photo-s/05/6e/dc/bf/bandung-zoo.jpg'),
(10, 6, 'http://4.bp.blogspot.com/-RPAHOH1kHpI/UfUlw3rD5MI/AAAAAAAACL0/9jxIrUXfqis/s1600/bandungview-babakan-siliwangi-jembatan-gantung-7.JPG'),
(11, 7, 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Sri_Baduga_Museum.JPG/800px-Sri_Baduga_Museum.JPG'),
(12, 8, 'http://sky-adventure.com/wp-content/uploads/2015/07/WISATA-KE-MUSEUM-KONFERENSI-ASIA-AFRIKA2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `obyek`
--

CREATE TABLE IF NOT EXISTS `obyek` (
  `obyek_kd` int(11) NOT NULL AUTO_INCREMENT,
  `obyek_nama` text NOT NULL,
  `obyek_deskripsi` text NOT NULL,
  `obyek_deskripsi_penuh` text NOT NULL,
  PRIMARY KEY (`obyek_kd`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `obyek`
--

INSERT INTO `obyek` (`obyek_kd`, `obyek_nama`, `obyek_deskripsi`, `obyek_deskripsi_penuh`) VALUES
(1, 'Patenggang Lake', 'Patenggang Lake is a lake located in the area of natural tourist attractions in the southern Bandung city, West Java, Indonesia, to be exact it is in Ciwidey village', 'Patenggang Lake (Indonesian: Situ Patenggang) is a lake located in the area of natural tourist attractions in the southern Bandung city, West Java, Indonesia, to be exact it is in Ciwidey village. Located at an altitude of 1600 meters above sea level, this lake has a very exotic landscape.\r\n\r\nOriginally, this lake is called Situ Patengan by the local society. Since the pronunciation of patengan is too difficult to say in Sundanese, then they call it Situ Patenggang or Patenggang Lake.\r\n\r\nThe label/name of the lake in maps.google.com is Situ Penganten which Penganten means newlywed couple.'),
(2, 'Gedung Sate', 'Gedung Sate is a public building in Bandung, West Java, Indonesia. It was designed according to a neoclassical design incorporating native Indonesian elements by Dutch architect J. Gerber to be the seat of the Dutch East Indies department of Transport, Public Works and Water Management', 'Gedung Sate is a public building in Bandung, West Java, Indonesia. It was designed according to a neoclassical design incorporating native Indonesian elements by Dutch architect J. Gerber to be the seat of the Dutch East Indies department of Transport, Public Works and Water Management'),
(3, 'Kawah Putih', 'Kawah Putih (English: White Crater) is a striking crater lake and tourist spot in a volcanic crater about 50 km south of Bandung in West Java in Indonesia.', 'Kawah Putih (English: White Crater) is a striking crater lake and tourist spot in a volcanic crater about 50 km south of Bandung in West Java in Indonesia.\r\n\r\nKawah Putih lake (7.10° S 107.24° E) is one of the two craters which make up Mount Patuha, an andesitic stratovolcano (a "composite" volcano). Mt Patuha is one of numerous volcanoes in Java. Kawah Putih crater lake itself represents a relatively stable volcanic system with no records of significant activity since around 1600.\r\n\r\nThe Kawah Putih site was opened to visitors in 1987. The lake is 2,430 meters above sea level so the local climate is often quite chilly (temperatures are frequently around 10 degrees Celsius). This makes a brisk change from the humidity of the north Java plain and the capital city of Jakarta. Kawah Putih is a sizeable highly acid lake (pH 0.5-1.3) which changes colour from bluish to whitish green, or brown, depending on the concentration of sulfur and the temperature or the oxidation state.[4] The sand and rocks surrounding the lake have been also leached into whitish colours through interaction with the acidic lake waters (with possible mineral precipitation as well).'),
(4, 'Tangkuban Perahu', 'Tangkuban Perahu is a stratovolcano 30 km north of the city of Bandung, the provincial capital of West Java, Indonesia. It erupted in 1826, 1829, 1842, 1846, 1896, 1910, 1926, 1929, 1952, 1957, 1961, 1965, 1967, 1969, 1983, and 2013', 'Tangkuban Perahu (spelt Tangkuban Parahu in the local Sundanese dialect) is a stratovolcano 30 km north of the city of Bandung, the provincial capital of West Java, Indonesia. It erupted in 1826, 1829, 1842, 1846, 1896, 1910, 1926, 1929, 1952, 1957, 1961, 1965, 1967, 1969, 1983, and 2013. It is a popular tourist attraction where tourists can hike or ride to the edge of the crater to view the hot water springs and boiling mud up close, and buy eggs cooked on the hot surface. Together with Mount Burangrang and Bukit Tunggul, it is a remnant of the ancient Mount Sunda after the plinian eruption caused the Caldera to collapse.\r\n\r\nIn April 2005 the Directorate of Volcanology and Geological Hazard Mitigation raised an alert, forbidding visitors from going up the volcano. "Sensors on the slopes of the two mountains - Anak Krakatoa on the southern tip of Sumatra Island and Tangkuban Perahu in Java - picked up an increase in volcanic activity and a build up of gases, said government volcanologist Syamsul Rizal." On the mountain''s northern flank is Death Valley, which derives its name from frequent accumulation of poisonous gases.'),
(5, 'Bandung Zoo', 'Kebun Binatang Bandung is a 14-hectare zoo located in the city of Bandung in West Java, Indonesia known for its mistreatment and abuse of the animals that reside there.', 'Kebun Binatang Bandung is a 14-hectare (35-acre) zoo located in the city of Bandung in West Java, Indonesia known for its mistreatment and abuse of the animals that reside there. It was created in 1933 when two existing zoos in the city (Cimindi zoo and Dago Atas zoo) were combined and moved to the current location on Taman Sari street. The new zoo was located in "Jubileum Park", a botanical garden created in 1923 to celebrate the silver Jubilee of Queen Wilhelmina of the Netherlands.\r\n\r\nThe zoo has garnered widespread negative publicity in recent years regarding its lack of proper animal care. The mayor of Bandung in 2015 stated that he did not possess the legal authority to close down the zoo, which is privately owned. Foreign tourists, as well as some locals, have described witnessing animals eating their own feces and other sights including visibly malnourished bears, deer with severe skin conditions, and animals shackled to the ground.[2] As of January 2017, an online petition had received over 585,000 signatures asking the Ministry of Environment and Forestry (Indonesia) to close the zoo and re-locate the animals to a conservation or sanctuary.'),
(6, 'Babakan Siliwangi', 'Babakan Siliwangi is a 3.8 acre urban forest in Bandung, Indonesia It is part of the green belt of Bandung and is a recreation place for the locals there. On 27 September, 2011, the United Nations certified Babakan Siliwangi as a world urban forest.', 'Babakan Siliwangi is a 3.8 acre urban forest in Bandung, Indonesia It is part of the green belt of Bandung and is a recreation place for the locals there. On 27 September, 2011, the United Nations certified Babakan Siliwangi as a world urban forest.'),
(7, 'Sri Baduga Museum', 'Sri Baduga Museum is a state museum located in Bandung, Indonesia. As a state museum, the museum features various items related with the province of West Java, such as Sundanese crafts, furnishings, geologic history, and natural diversity.', 'Sri Baduga Museum is a state museum located in Bandung, Indonesia. As a state museum, the museum features various items related with the province of West Java, such as Sundanese crafts, furnishings, geologic history, and natural diversity.\r\n\r\nSri Baduga Museum is a state museum located in Bandung, Indonesia. As a state museum, the museum features various items related with the province of West Java, such as Sundanese crafts, furnishings, geologic history, and natural diversity.'),
(8, 'Museum Konferensi Asia Afrika', 'Museum dedicated to the 1955 Bandung Conference between newly–independent African & Asian states.', 'Museum dedicated to the 1955 Bandung Conference between newly–independent African & Asian states.');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `petugas_kd` int(11) NOT NULL AUTO_INCREMENT,
  `petugas_nama` text NOT NULL,
  `petugas_akun` text NOT NULL,
  `petugas_katasandi` text NOT NULL,
  PRIMARY KEY (`petugas_kd`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`petugas_kd`, `petugas_nama`, `petugas_akun`, `petugas_katasandi`) VALUES
(1, 'Ali Qornan', 'qornanali', 'aliqornan');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
