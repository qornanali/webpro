<head>
<title>
  <?php
  $redirect = 0;
    if(isset($_GET['nav'])){
      if($_GET['nav'] == "login"){
        $redirect = 1;
          $title = "Masuk";
      }else if($_GET['nav'] == "catalog"){
        if(isset($_GET['id'])){
          $redirect = 4;
        }else {
          $redirect = 2;
        }
        $title = "Obyek Wisata";
      }else if($_GET['nav'] == "new_object"){
          $redirect = 5;
          $title = "Tambah obyek wisata baru";
      }else{
        $title = "Beranda";
      }
      echo "Pariwisata - " . $title;
    }
     ?>
</title/>
</head>
<div class="row">
<?php
include 'navbar.php';
// include 'tab.php';
?>
</div>
<body>
  <div class="row">
  <?php
    switch($redirect){
      case 1 :
      include 'login.php';
      break;
      case 2 :
      include 'catalog.php';
      break;
      case 4 :
      include "catalog_detail.php";
      break;
      case 5 :
      include 'new_object.php';
      break;
      default :
      include 'home.php';
      break;
    }
    ?>
</div>
<?php
include 'footer.php'
    ?>
</body>
