<nav class="navbar navbar-default">
 <div class="container-fluid">
   <div class="navbar-header">
     <a class="navbar-brand" href="index.php?nav=home">Pariwisata</a>
   </div>
   <ul class="nav navbar-nav navbar-left">
     <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'catalog' ?  "active" : "") : (""); ?>>
       <a href="index.php?nav=catalog">Obyek Wisata</a>
     </li>
     <form class="navbar-form navbar-left">
       <div class="form-group">
         <input type="text" class="form-control" placeholder="Search">
       </div>
       <button type="submit" class="btn btn-default">Cari</button>
     </form>
   <!-- <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'cart' ?  "active" : "") : (""); ?>>
     <a href="index.php?nav=cart">Keranjangku</a>
   </li>-->
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'blog' ?  "active" : "") : (""); ?>>
        <a href="index.php?nav=blog">Blog</a>
      </li>
      <?php if(!isset($_SESSION['kd'])){ ?>
    <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'login' ?  "active" : "") : (""); ?>>
      <a href="index.php?nav=login">Masuk</a>
    </li>
    <?php } else {?>
    <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['nama']; ?>
       <span class="caret"></span></a>
       <ul class="dropdown-menu">
         <li><a href="index.php?nav=profile">Ubah profil</a></li>
          <li><a href="index.php?nav=new_object">Tambah Obyek Wisata Baru</a></li>
         <li><a href="controller/do_logout.php">Keluar</a></li>
       </ul>
     </li>
     <?php }?>
     </ul>
 </div>
</nav>
