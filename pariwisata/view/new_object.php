
<div class="container col-sm10">
<form method="post" action="controller/do_new_object.php" enctype="multipart/form-data">
 <div class="form-group">
   <label for="nama">Nama Obyek Wisata:</label>
   <input type="text" class="form-control" name="nama">
 </div>
 <div class="form-group">
   <label for="desk">Deskripsi:</label>
   <textarea class="form-control" rows="3" name="desk"></textarea>
 </div>
 <div class="form-group">
   <label for="desk_penuh">Deskripsi Penuh:</label>
   <textarea  class="form-control" rows="6" name="desk_penuh"></textarea>
 </div>
 <div class="form-group">
   <label for="image">Gambar:</label>
  <input type="file" name="image" id="image">
 </div>
 <button type="submit" class="btn btn-default">Simpan</button>
</form>
</div>
