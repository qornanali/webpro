<div class="container col-sm10">
<?php 
if(isset($_GET['error'])){
  $errorId = $_GET['error'];
  $errorMsg;
  if($errorId == 1){
    $errorMsg = "Email/Password yang anda masukkan salah atau tidak terdaftar!";
  }else if($errorId == 2){
    $errorMsg = "Captcha yang anda masukkan salah!";
  }
?>
<div class="alert alert-danger">
  <strong>Terjadi kesalahan.</strong> <?php echo $errorMsg; ?>
</div>
<?php
}
?>
<form method="post" action="controller/do_login.php">
 <div class="form-group">
   <label for="email">Email:</label>
   <input type="text" class="form-control" name="email">
 </div>
 <div class="form-group">
   <label for="password">Password:</label>
   <input type="password" class="form-control" name="password">
 </div>
   <div class="g-recaptcha form-group" data-sitekey="6LfF-yIUAAAAAHm_tRQHfHGMgHz4vqgaMtO3YWSa"></div>
 <button type="submit" class="btn btn-default">Masuk</button>
</form>
</div>