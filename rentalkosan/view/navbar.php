<nav class="navbar navbar-inverse navbar-fixed-bottom">
 <div class="container-fluid">
   <div class="navbar-header">
     <a class="navbar-brand" href="index.php?nav=home">Rental Kosan</a>
   </div>
   <ul class="nav navbar-nav navbar-left">
     <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'catalog' ?  "active" : "") : (""); ?>>
       <a href="index.php?nav=catalog">Katalog</a>
     </li>
     <form class="navbar-form navbar-left">
       <div class="form-group">
         <input type="text" class="form-control" placeholder="Nama kosan atau lokasi..">
       </div>
       <button type="submit" class="btn btn-default">Cari</button>
     </form>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <!--<li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'blog' ?  "active" : "") : (""); ?>>
        <a href="index.php?nav=blog">Blog</a>
      </li>-->
      <?php if(!isset($_SESSION['email'])){ ?>
    <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'login' ?  "active" : "") : (""); ?>>
      <a href="index.php?nav=login"><span class="glyphicon glyphicon-log-in"></span> Masuk/Daftar</a>
    </li>
    <?php } else {?>
    <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'cart' ?  "active" : "") : (""); ?>>
      <a href="index.php?nav=cart"><span class="glyphicon glyphicon-shopping-cart"></span> Keranjang <span class="badge">2</span></a>
    </li>
    <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['name']; ?>
       <span class="caret"></span></a>
       <ul class="dropdown-menu">
         <li><a href="index.php?nav=profile">Profil</a></li>
         <li><a href="index.php?nav=add_kosan">Jual Kosan</a></li>
         <li><a href="controller/do_logout.php">Keluar</a></li>
       </ul>
     </li>
     <?php }?>
     </ul>
 </div>
</nav>
