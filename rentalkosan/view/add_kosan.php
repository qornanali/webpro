<?php if(isset($_SESSION['email'])){ ?>
<div class="container col-sm10">
<form method="post" action="controller/do_add_kosan.php" enctype="multipart/form-data">
 <div class="form-group">
   <label for="paint_title">Nama Kosan:</label>
   <input type="text" class="form-control" name="kosan_name">
 </div>
 <div class="form-group">
   <label for="paint_shortdesc">Deskripsi :</label>
   <textarea class="form-control" rows="3" name="kosan_desc"></textarea>
 </div>
  <div class="form-group">
   <label for="paint_title">Luas:</label>
   <input type="text" class="form-control" name="kosan_large">
 </div>
 <div class="form-group">
   <label for="desk_penuh">Alamat :</label>
   <textarea  class="form-control" rows="3" name="kosan_address"></textarea>
 </div>
 <div class="form-group">
   <label for="paint_title">Harga:</label>
   <input type="text" class="form-control" name="kosan_price">
 </div>
 <div class="form-group">
   <label for="fileToUpload">Foto:</label>
  <input type="file" name="fileToUpload" id="fileToUpload">
 </div>
 <button type="submit" class="btn btn-default">OK</button>
</form>
</div>
<?php } 
  else {
      header('location:../index.php?nav=home');
  }
?>
