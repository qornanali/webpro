-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 12, 2017 at 12:58 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_belikosan`
--

-- --------------------------------------------------------

--
-- Table structure for table `kosan`
--

CREATE TABLE IF NOT EXISTS `kosan` (
  `kosan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kosan_name` text NOT NULL,
  `kosan_desc` text NOT NULL,
  `kosan_large` text NOT NULL,
  `kosan_address` text NOT NULL,
  `kosan_photo` text NOT NULL,
  `kosan_price` double NOT NULL,
  `kosan_createdate` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`kosan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kosan`
--

INSERT INTO `kosan` (`kosan_id`, `kosan_name`, `kosan_desc`, `kosan_large`, `kosan_address`, `kosan_photo`, `kosan_price`, `kosan_createdate`, `user_id`) VALUES
(2, 'Mabes Kost', 'Pemilik rumah ini bernama ibu Cucu tinggal di Jakarta yang merupakan adik dari Ibu Imas yang bisa dibilang beliaulah ibu kost kami. Sejak 2006 mayoritas penghuni kost ini merupakan mahasiswa jurusan Akuntansi Prodi D3 Akuntansi dan D4 Akuntansi Manajemen Pemerintahan, maka dari itu banyak peninggalan buku-buku, modul dan bahan kuliah akuntansi lainnya yang diwariskan dan masih terpakai, dan itulah alasannya kost-an ini sering kami sebut MABES HMAK. Mulai 2013 penghuni kost ini mulai campuran dari berbagai jurusan baik itu Telkom, TI, Aero, dll.  Fasilitas rumah yang tersedia terdiri dari : -5 Kamar luas (up to 3 orng/kamar, 3 kamar di lt.1 dan 2 kamar di lt.2 -Internet Wi-fiIndieHome (Speedy) kecepatan download sampai dengan 500kb/s ping stabil -Listrik 1300 watt -Kamar Mandi 2 -Ruang tamu luas lt.1 dan lt.2 (biasa digunakan utk kumpul/maen/kerja kelompok/belajar utk UAS&UTS/rapat organisasi) -Ruang dapur -Tempat Parkir di depan rumah (dalam gerbang) 5 s/d 7 motor -Tempat Parkir di dalam rumah 4 s/d 5 motor -Perpustakaan mini -Jemur pakaian lt 2 -Kolam ikan (saat ini tidak difungsikan).', '12 m²', 'Kecamatan Parongpong, Desa Ciwaruga, Jalan Ciwaruga no.61 Rt.01 Rw.06', 'http://www.informasikost.com/wp-content/uploads/2016/01/Kosan-Baladewa-6.jpg', 300000, '2017-06-12', 1),
(3, 'Kos Puri Syailendra', 'Lokasi yang strategis, cocok untuk karyawan maupun mahasiswa. Mudah mendapatkan angkutan umum. Kurang lebih 5-10 menit dari Universitas Maranatha, dekat dengan toko dan minimarket. Hanya 1,3km ke pintu Tol Pasteur dan 4km dari jalan layang Pasupati. ', '10 m²', 'Jl. Terusan Setraria 3 Kav. 12 Lemahneundeut, Bandung', 'http://dsc.ibilik.com/photos/3156588/DSC01082_-_Copy_view.JPG', 500000, '2017-06-12', 1),
(4, 'Kost Pondok KeCe', 'empat Strategis:\r\n5 menit ke Ciwalk\r\n5 menit ke jalan raya Cipaganti\r\n10 menit ke PVJ\r\n10 menit ke ITB\r\n10 menit, ke Setia budi\r\n15 menit ke UNPAD\r\n\r\nCocok Untuk Mahasiswi/Karyawati yang beraktivitas di Cipaganti/Cihampelas/\r\nSetiabudi/Sukajadi dan Sekitarnya\r\n\r\nFasilitas:\r\n-Tempat Tidur (untuk 1orang)\r\n-Gordeng/Jendela\r\n-Kamar mandi dalam (Shower dan Closet)\r\n-Jemuran\r\n\r\n+++\r\n(jika ingin AC bisa kami pasangkan & harga disesuaikan)\r\n(jika ingin tv bisa kami pasangkan & harga disesuaikan)\r\n***Bisa kosongan', '3 m2', 'Jl. Kaum Cipaganti III No. 50\r\nBandung', 'http://dsc.ibilik.com/photos/3118071/kost_view.jpg', 800000, '2017-06-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` text NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` text NOT NULL,
  `user_phone` varchar(14) NOT NULL,
  `user_address` text NOT NULL,
  `user_joindate` date NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`,`user_phone`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_phone`, `user_address`, `user_joindate`) VALUES
(1, 'Ali Qornan', 'aliqornan@ymail.com', 'e45836871bcbe478136208dd4dde9b80', '085861574768', 'Jl. Batu alam 3', '2017-06-12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
