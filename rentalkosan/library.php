<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="../w3css/w3.css">
<link rel="stylesheet" href="/css/navbar.css">
<script src="../bootstrap-3.3.7-dist/js/jquery.js"></script>
<script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script> 
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
