<?php
session_start();
include '../service/dbconnection.php';

$title= $_POST['paint_title'];
$short_desc = $_POST['paint_shortdesc'];
$full_desc = $_POST['paint_fulldesc'];
$user_id = $_SESSION['id'];
$create_date = date_format(date_create($_POST['paint_createdate']), 'Y-m-d H:i:s');

$target_file = "uploads/" . basename($_FILES["fileToUpload"]["name"]);
$filepath = "../" . $target_file;
$uploadOk = 1;
$fileToUploadFileType = pathinfo($filepath,PATHINFO_EXTENSION);

if($fileToUploadFileType != "jpg" && $fileToUploadFileType != "png" && $fileToUploadFileType != "jpeg"
&& $fileToUploadFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $filepath)) {
        $sql = "INSERT INTO photo (user_id, photo_title, photo_shortdesc, photo_fulldesc, photo_image, photo_createdate) VALUES ('$user_id', '$title', '$short_desc','$full_desc', '$target_file', '$create_date');";
        $result = mysqli_query($connection, $sql);
        $data = mysqli_fetch_array($result, MYSQL_ASSOC);
        if(!mysqli_connect_error()){
            header('location:../index.php');
        }else{
            echo "Failed";
        }
         
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
