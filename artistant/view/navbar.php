<nav class="navbar navbar-inverse">
 <div class="container-fluid">
   <div class="navbar-header">
     <a class="navbar-brand" href="index.php?nav=home">Artistant</a>
   </div>
   <ul class="nav navbar-nav navbar-left">
     <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'gallery' ?  "active" : "") : (""); ?>>
       <a href="index.php?nav=gallery">Gallery</a>
     </li>
     <form class="navbar-form navbar-left">
       <div class="form-group">
         <input type="text" class="form-control" placeholder="Title or artist's name..">
       </div>
       <button type="submit" class="btn btn-default">Search</button>
     </form>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'blog' ?  "active" : "") : (""); ?>>
        <a href="index.php?nav=blog">Blog</a>
      </li>
      <?php if(!isset($_SESSION['accountname'])){ ?>
    <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'login' ?  "active" : "") : (""); ?>>
      <a href="index.php?nav=login">Login</a>
    </li>
    <?php } else {?>
    <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['fullname']; ?>
       <span class="caret"></span></a>
       <ul class="dropdown-menu">
         <li><a href="index.php?nav=profile">My profile</a></li>
         <li><a href="index.php?nav=add_paint">Add new paint</a></li>
         <li><a href="controller/do_logout.php">Logout</a></li>
       </ul>
     </li>
     <?php }?>
     </ul>
 </div>
</nav>
