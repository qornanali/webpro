<!--<div class="col-md-10">
<?php
  $sql = "SELECT photo.photo_id,photo.photo_image FROM photo ORDER BY RAND() LIMIT 4";
  $listPhoto = mysqli_query($connection, $sql);
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
  <?php 
  $i = 0;
  while($data = mysqli_fetch_array($listPhoto, MYSQL_ASSOC)){
    ?>
    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0 ? "active" : "");?>"></li>
    <?php $i++; } ?>
  </ol> 
  <div class="carousel-inner">
  <?php 
  $i = 0;
  while($data = mysqli_fetch_array($listPhoto, MYSQL_ASSOC)){
    ?>
    <div class="item <?php echo ($i == 0 ? "active" : "");?>">
      <img src="<?php echo $data['photo_image']; ?>" alt="<?php echo $data['photo_title']; ?>">
      <div class="carousel-caption">
        <h3><?php echo $data['photo_title']; ?></h3>
        <p><?php echo $data['photo_shortdesc']; ?></p>
      </div>
    </div>
     <?php $i++; } ?>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>-->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="../bootstrap-3.3.7-dist/image/la.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="../bootstrap-3.3.7-dist/image/chicago.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="../bootstrap-3.3.7-dist/image/ny.jpg" alt="New york" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>