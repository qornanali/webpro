<?php if(isset($_SESSION['accountname'])){ ?>
<div class="container col-sm10">
<form method="post" action="controller/do_add_photo.php" enctype="multipart/form-data">
 <div class="form-group">
   <label for="paint_title">Paint Title :</label>
   <input type="text" class="form-control" name="paint_title">
 </div>
 <div class="form-group">
   <label for="datepicker">Paint Create Date :</label>
    <input class="form-control" type="date" name="paint_createdate">
 </div>
 <div class="form-group">
   <label for="paint_shortdesc">Short Description :</label>
   <textarea class="form-control" rows="3" name="paint_shortdesc"></textarea>
 </div>
 <div class="form-group">
   <label for="desk_penuh">Full Description :</label>
   <textarea  class="form-control" rows="6" name="paint_fulldesc"></textarea>
 </div>
 <div class="form-group">
   <label for="fileToUpload">Image:</label>
  <input type="file" name="fileToUpload" id="fileToUpload">
 </div>
 <button type="submit" class="btn btn-default">Upload</button>
</form>
</div>
<?php } 
// else {
//     header('location:../index.php?nav=home');
// }
?>
