<head>
<title>
  <?php
  $redirect = 0;
    if(isset($_GET['nav'])){
      if($_GET['nav'] == "login"){
        $redirect = 1;
          $title = "login";
      }else if($_GET['nav'] == "gallery"){
        if(isset($_GET['id'])){
          $redirect = 4;
        }else {
          $redirect = 2;
        }
        $title = "Gallery";
      }else if($_GET['nav'] == "add_paint"){
          $redirect = 5;
          $title = "Add new paint";
      }else{
        $title = "Home";
      }
      echo "Artistant - " . $title;
    }else{
      echo "Artistant - Home";
    }
     ?>
</title/>
</head>
<body>
<?php
include 'navbar.php';
?>
  <div class="container">
  <div class="row">
  <?php
    switch($redirect){
      case 1 :
      include 'login.php';
      break;
      case 2 :
      include 'gallery.php';
      break;
      case 4 :
      include "gallery_detail.php";
      break;
      case 5 :
      include 'add_paint.php';
      break;
        default :
        include 'home.php';
        break;
    }
    ?>
</div>
<?php
// include 'footer.php'
    ?>
    </div>
</body>
