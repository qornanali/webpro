<?php

$field = isset($_GET['field']) ? $_GET['field'] : "nama";
$query = isset($_GET['query']) ? $_GET['query'] : "";
$sort = isset($_GET['sort']) ? $_GET['sort'] : "ASC";
$order = isset($_GET['order']) ? $_GET['order'] : "nim";

?>

Cari Mahasiswa
<form name="form_cari_mahasiswa" method="get" action="index.php">
    <table>
    <tr>
      <input type="hidden" name="nav" value="mahasiswa" />
      <td>
        <select name="field">
          <option value="nama"
          <?php if(isset($_GET['field'])){
            echo $_GET['field'] == "nama" ? "selected" : "";
           }
            ?>>Nama
          </option>
          <option value="nim"
          <?php if(isset($_GET['field'])){
            echo $_GET['field'] == "nim" ? "selected" : "";
           }
            ?>>Nim
          </option>
          </select>
      </td>
      <td>
        <input name="query" type="text"
        value=<?php if(isset($_GET['query'])){
          echo "'" . $_GET['query'] . "'";
        }?>>
      </td>
      <td>
        Urut berdasarkan
      </td>
      <td>
        <select name="order">
          <option value="nim"
          <?php if(isset($_GET['order'])){
            echo $_GET['order'] == "nim" ? "selected" : "";
           }
            ?>>Nim
          </option>
          <option value="nama"
          <?php if(isset($_GET['order'])){
            echo $_GET['order'] == "nama" ? "selected" : "";
           }
            ?>>Nama
          </option>
          <option value="umur"
          <?php if(isset($_GET['order'])){
            echo $_GET['order'] == "umur" ? "selected" : "";
           }
            ?>>Umur
          </option>
        </select>
      </td>
      <td>
        <select name="sort">
          <option value="ASC"
          <?php if(isset($_GET['sort'])){
            echo $_GET['sort'] == "asc" || $_GET['sort'] == "ASC" ? "selected" : "";
           }
            ?>>A-Z
          </option>
          <option value="DESC"
          <?php if(isset($_GET['sort'])){
            echo $_GET['sort'] == "desc" || $_GET['sort'] == "DESC" ? "selected" : "";
           }
            ?>>Z-A
          </option>
        </select>
      </td>
      <td>
         <input type="submit" value="Cari">Cari</input>
      </td>
    </tr>
    </table>
</form>
<a href="index.php?nav=mahasiswa&view=form"><button>Tambah Data Mahasiswa</button></a>
<br><br>
<table border="0.5" width="100%" align="center">
    <thead  bgcolor="#3949ab">
        <th style="color : #fff;" width="10%">Nim</th>
        <th style="color : #fff;" width="20%">Foto</th>
        <th style="color : #fff;" width="40%">Nama</th>
        <th style="color : #fff;" width="10%">Umur</th>
        <th style="color : #fff;" width="20%">Aksi</th>
    </thead>
<?php

include 'koneksidb.php';

$sql = "SELECT * FROM mahasiswa WHERE " . $field . " like '%". $query ."%' ORDER BY " . $order . " " . $sort;

$listMahasiswa = mysqli_query($connection, $sql);
if($listMahasiswa->num_rows == 0){
    echo "0 results <br>";
    }else{
        while($data = mysqli_fetch_array($listMahasiswa, MYSQL_ASSOC)){
    ?>
    <tr>
        <td align="center"><?php echo $data['nim']; ?></td>
        <td align="center"><img src=<?php echo "http://akademik.polban.ac.id/foto/". $data['nim']   . ".JPG" ?> width="40px"  /></td>
        <td><?php echo $data['nama']; ?></td>
        <td  align="center"><?php echo $data['umur']; ?></td>
        <td  align="center">
          <a href=<?php echo "mahasiswa_hapus.php?nim=" . $data['nim'] ?>  onclick="return confirm('Apa anda yakin?');">
            <button>Hapus</button></a>
          <a href=<?php echo "index.php?nav=mahasiswa&view=form&nim=" . $data['nim'] ?>>
            <button>Ubah</button></a>
          <a href=<?php echo "index.php?nav=mahasiswa&view=detail&nim=" . $data['nim'] ?>>
            <button>Detail</button>
        </td>
    </tr>
    <?php }
    } ?>
</table>
<a href="index.php?nav=mahasiswa&view=form"><button>Tambah Data Mahasiswa</button></a>
