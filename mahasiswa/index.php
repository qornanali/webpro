<?php
$redirect = 0;
  if(isset($_GET['nav'])){
    if($_GET['nav'] == "info"){
      $redirect = 1;
    }else if(isset($_GET['view'])){
      if($_GET['view'] == "detail"){
        $redirect = 3;
      }else if($_GET['view'] == "form"){
        $redirect = 4;
      }
    }else if($_GET['nav'] == "mahasiswa"){
      $redirect = 2;
    }
  }
  ?>
  <html>
  <head>
    <title>
      Data Mahasiswa
    </title>
  </head>
  <body>
    <div style="background-color : #3F51B5; color : white; padding : 40px;" align="center">
      <h1>Data Mahasiswa</h1>
    </div>
    <table border="0">
      <tr>
        <td><div><a href="index.php?nav=home">Home</a></div></td>
        <td><div><a href="index.php?nav=info">Info</a></div></td>
        <td><div><a href="index.php?nav=mahasiswa">Mahasiswa</a></div></td>
      </tr>
    </table>
    <br>
    <?php
    switch($redirect){
      case 1 :
        include 'info.php';
      break;
      case 2 :
        include 'mahasiswa.php';
      break;
      case 3 :
        include 'mahasiswa_detail.php';
      break;
      case 4 :
        include 'form_mahasiswa.php';
      break;
      default :
        include 'home.php';
      break;
    }
    ?>
    <div style="background-color : #5C6BC0; color : white; padding : 20px; margin : 20px 0px 0px 0px" align="center">
      Made with &#9829 in Ciwaruga by Ali Qornan Jaisyurrahman
    </div>
    <a href="https://gitlab.com/qornanali/webpro">Source Code</a>
  </body>
</html>
