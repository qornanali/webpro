<?php

$field = isset($_GET['field']) ? $_GET['field'] : "nama";
$query = isset($_GET['query']) ? $_GET['query'] : "";
$sort = isset($_GET['sort']) ? $_GET['sort'] : "ASC";
$order = isset($_GET['order']) ? $_GET['order'] : "nim";
$page = isset($_GET['page']) ? $_GET['page'] : 1;

$start = (($page-1) * 10);
$end = ($page * 10);

$n = 0;

$url = "index.php?nav=mahasiswa&field=" . $field . "&query="
. $query . "&sort=" . $sort . "&order=" . $order . "&page=";

$count_result = mysqli_query($connection, "SELECT COUNT(mahasiswa.nim) AS jumlah FROM mahasiswa");
while($data = mysqli_fetch_array($count_result, MYSQL_ASSOC)){
  $n = $data['jumlah'];
}

$sql = "SELECT mahasiswa.nim, mahasiswa.nama, mahasiswa.umur FROM mahasiswa WHERE " . $field . " like '%". $query ."%' "
. " ORDER BY " . $order . " " . $sort . " LIMIT 10 OFFSET " . $start;

$listMahasiswa = mysqli_query($connection, $sql);

// echo $sql;

?>
<div class="row">
  <div class="col-sm-12"><h4>Cari Mahasiswa</h4></div>
</div>
<div class="form-group">
  <form name="form_cari_mahasiswa" method="get" action="index.php">
      <table>
      <tr>
        <input type="hidden" name="nav" value="mahasiswa" />
        <td>
          <select name="field">
            <option value="nama"
            <?php if(isset($_GET['field'])){
              echo $_GET['field'] == "nama" ? "selected" : "";
             }
              ?>>Nama
            </option>
            <option value="nim"
            <?php if(isset($_GET['field'])){
              echo $_GET['field'] == "nim" ? "selected" : "";
             }
              ?>>Nim
            </option>
            </select>
        </td>
        <td>
          <input name="query" type="text"
          value=<?php if(isset($_GET['query'])){
            echo "'" . $_GET['query'] . "'";
          }?>>
        </td>
        <td>
          Urut berdasarkan
        </td>
        <td>
          <select name="order">
            <option value="nim"
            <?php if(isset($_GET['order'])){
              echo $_GET['order'] == "nim" ? "selected" : "";
             }
              ?>>Nim
            </option>
            <option value="nama"
            <?php if(isset($_GET['order'])){
              echo $_GET['order'] == "nama" ? "selected" : "";
             }
              ?>>Nama
            </option>
          </select>
        </td>
        <td>
          <select name="sort">
            <option value="ASC"
            <?php if(isset($_GET['sort'])){
              echo $_GET['sort'] == "asc" || $_GET['sort'] == "ASC" ? "selected" : "";
             }
              ?>>A-Z
            </option>
            <option value="DESC"
            <?php if(isset($_GET['sort'])){
              echo $_GET['sort'] == "desc" || $_GET['sort'] == "DESC" ? "selected" : "";
             }
              ?>>Z-A
            </option>
          </select>
        </td>
        <td>
           <input type="submit"  class="btn btn-default" value="cari"></input>
        </td>
      </tr>
      </table>
  </form>
</div>
<div class="row">
<div class="col-sm-4">
<ul class="pagination">
  <?php if ($page > 1) { ?>
  <li><a href=<?php echo $url . ($page-1);?>>❮</a></li>
  <li><a href=<?php echo $url . ($page-1);?>><?php echo ($page-1);?></a></li>
  <?php } ?>
  <li><a href=<?php echo $url . ($page);?>><?php echo ($page);?></a></li>
  <?php if ($n > $end) { ?>
    <li><a href=<?php echo $url . ($page+1);?>><?php echo ($page+1);?></a></li>
    <li><a href=<?php echo $url . ($page+1);?>>❯</a></li>
    <?php } ?>
</ul>
</div>
<div class="col-sm-4">
  <?php if(isset($_SESSION['nim'])) {?>
<a href="index.php?nav=mahasiswa&view=form"><button  class="btn btn-default">Tambah Data Mahasiswa</button></a>
<?php } ?>
</div>
</div>
<br><br>
<table class="table table-striped">
    <thead>
        <th width="20%">Nim</th>
        <th width="60%">Mahasiswa</th>
        <th width="20%">Aksi</th>
    </thead>
<?php
if($listMahasiswa->num_rows == 0){
    echo "0 results <br>";
    }else{
        while($data = mysqli_fetch_array($listMahasiswa, MYSQL_ASSOC)){
    ?>
    <tr>
        <td><?php echo $data['nim']; ?></td>
        <td>
        <div class="col-sm-2">
          <img class="img-responsive" src=<?php echo "http://akademik.polban.ac.id/foto/". $data['nim']   . ".JPG" ?> width="30px"  />
        </div>
        <div class="col-sm-8">
          <?php echo $data['nama']; ?>
        </div>
        </td>
        <td>
          <?php if(isset($_SESSION['nim'])) {?>
          <a href=<?php echo "controller/crud/mahasiswa_hapus.php?nim=" . $data['nim'] ?>  onclick="return confirm('Apa anda yakin?');">
            <button class="btn btn-default">Hapus</button></a> <?php } ?>
            <?php if(isset($_SESSION['nim'])) {?>
          <a href=<?php echo "index.php?nav=mahasiswa&view=form&nim=" . $data['nim'] ?>>
            <button class="btn btn-default">Ubah</button></a> <?php } ?>
          <a href=<?php echo "index.php?nav=mahasiswa&view=detail&nim=" . $data['nim'] ?>>
            <button class="btn btn-default">Detail</button>
        </td>
    </tr>
    <?php }
    } ?>
</table>
<ul class="pagination">
  <?php if ($page > 1) { ?>
  <li><a href=<?php echo $url . ($page-1);?>>❮</a></li>
  <li><a href=<?php echo $url . ($page-1);?>><?php echo ($page-1);?></a></li>
  <?php } ?>
  <li><a href=<?php echo $url . ($page);?>><?php echo ($page);?></a></li>
  <?php if ($n > $end) { ?>
    <li><a href=<?php echo $url . ($page+1);?>><?php echo ($page+1);?></a></li>
    <li><a href=<?php echo $url . ($page+1);?>>❯</a></li>
    <?php } ?>
</ul>
