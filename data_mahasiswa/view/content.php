<!-- Page Content -->
<?php
$redirect = 0;
  if(isset($_GET['nav'])){
    if($_GET['nav'] == "info"){
      $redirect = 1;
    }else if(isset($_GET['view'])){
      if($_GET['view'] == "detail"){
        $redirect = 3;
      }else if($_GET['view'] == "form"){
        $redirect = 4;
      }
    }else if($_GET['nav'] == "mahasiswa"){
      $redirect = 2;
    }else if($_GET['nav'] == "login"){
      $redirect = 5;
    }
  }
  ?>

<div id="page-content-wrapper">
    <div class="container-fluid">
        <d  iv class="row">
            <div class="col-lg-1">
                <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">&#9776;</a>
            </div>
            <div class="col-lg-12">
              <h2>
                <?php
                switch($redirect){
                case 1 :
                  echo "Info";
                break;
                case 2 :
                  echo "Mahasiswa";
                break;
                case 5 :
                  echo "Login";
                break;
                default :
                  echo "";
                break;
              }
              ?>
            </h2>
            </div>
          </div>
    <div class="row">
    <?php
    switch($redirect){
      case 1 :
      include 'info.php';
      break;
      case 2 :
      include 'mahasiswa.php';
      break;
      case 3 :
      include 'mahasiswa_detail.php';
      break;
      case 4 :
      include 'form_mahasiswa.php';
      break;
      case 5 :
      include 'mahasiswa_login.php';
      break;
      default :
      include 'home.php';
      break;
    }
    ?>
          <div class="col-lg-12">
    </div>
    </div>
</div>
</div>
<!-- /#page-content-wrapper -->
