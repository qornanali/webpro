-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2017 at 08:10 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_akademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim` varchar(12) NOT NULL,
  `password` text,
  `nama` varchar(60) DEFAULT NULL,
  `umur` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `password`, `nama`, `umur`) VALUES
('151511001', 'agan', 'Abdullah Geneardi Arshala Nugraha', '21'),
('151511002', NULL, 'Adi Maulana Triadi', '20'),
('151511003', NULL, 'Adinda Meilani', '19'),
('151511004', NULL, 'Ahmad Sufyan Atstsauri', '19'),
('151511005', NULL, 'Akhmad Nawawi', '20'),
('151511006', NULL, 'Al Pepa Indra', '20'),
('151511007', 'ali', 'Ali Qornan Jaisyurrahman', '19'),
('151511008', NULL, 'Auliya Raka Pratama', '20'),
('151511009', NULL, 'Bicky Eric Kantona', '19'),
('151511010', NULL, 'Dyas Nurhakim Sudiana', '20'),
('151511011', NULL, 'Eka Fitriyah Agustin', '19'),
('151511012', NULL, 'Husen Malik', '19'),
('151511013', NULL, 'Ikhsan Hari Wijayanto', '20'),
('151511014', NULL, 'Irmawati Indriani', '20'),
('151511015', NULL, 'Lulu Luthfiyatul Azkia', '20'),
('151511016', NULL, 'Mochamad Sohibul Iman', '20'),
('151511017', NULL, 'Mohamad Hamzhya Salsatinnov Hairy', '21'),
('151511018', NULL, 'Mohammad Jihannudin Akhsan', '19'),
('151511019', NULL, 'Muhamad Yuwan Aditya Ridwan', '20'),
('151511020', NULL, 'Muhammad Satria Prawira', '20'),
('151511021', NULL, 'Nada Dwi Nurafifah', '20'),
('151511022', NULL, 'Nur Rachmatika Sidik', '19'),
('151511023', NULL, 'Regita Safira', '19'),
('151511024', NULL, 'Reyvan Adryan Rohmatulloh', '21'),
('151511025', NULL, 'Rifqi Muhammad Fauzi', '20'),
('151511026', NULL, 'Rikaz Fawaiz', '20'),
('151511027', NULL, 'Rio Novirdian Abishah Putra', '20'),
('151511028', NULL, 'Riyan Gandarma Ramadhan', '19'),
('151511029', NULL, 'Sadan Fitroni', '19'),
('151511030', NULL, 'Syifana Nurahmi', '20'),
('151511031', NULL, 'Telaumbanua, Husein Abdullah', '21'),
('151511032', NULL, 'Tubagus Musyaffa', '20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
 ADD PRIMARY KEY (`nim`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
